import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DhmeComponent } from './dhme.component';

describe('DhmeComponent', () => {
  let component: DhmeComponent;
  let fixture: ComponentFixture<DhmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DhmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DhmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
