import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RhmeComponent } from './rhme.component';

describe('RhmeComponent', () => {
  let component: RhmeComponent;
  let fixture: ComponentFixture<RhmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RhmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RhmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
