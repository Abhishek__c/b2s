import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RnavComponent } from './rnav.component';

describe('RnavComponent', () => {
  let component: RnavComponent;
  let fixture: ComponentFixture<RnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
