import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RqstComponent } from './rqst.component';

describe('RqstComponent', () => {
  let component: RqstComponent;
  let fixture: ComponentFixture<RqstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RqstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RqstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
