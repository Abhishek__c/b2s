import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DntliveComponent } from './dntlive.component';

describe('DntliveComponent', () => {
  let component: DntliveComponent;
  let fixture: ComponentFixture<DntliveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DntliveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DntliveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
