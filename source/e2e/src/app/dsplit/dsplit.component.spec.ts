import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DsplitComponent } from './dsplit.component';

describe('DsplitComponent', () => {
  let component: DsplitComponent;
  let fixture: ComponentFixture<DsplitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DsplitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DsplitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
