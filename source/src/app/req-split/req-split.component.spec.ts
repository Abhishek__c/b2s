import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqSplitComponent } from './req-split.component';

describe('ReqSplitComponent', () => {
  let component: ReqSplitComponent;
  let fixture: ComponentFixture<ReqSplitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqSplitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqSplitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
