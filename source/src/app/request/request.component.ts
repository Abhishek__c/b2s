import { Component, OnInit, TemplateRef ,ViewChild} from '@angular/core';
import{BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  numbers: any;
  modalRef: BsModalRef;

  @ViewChild('f') signupform: NgForm;
  elements2: Array<{itemname: string,category: string,quantity:string}>= [];
  div1= false;

   constructor(private modalService: BsModalService) {
     this.numbers = new Array(10).fill(100).map((x,i)=>i); // [0,1,2,3,4,...,100]
   }
   ngOnInit(): void {

   }
   openModal(template: TemplateRef<any>) {
     this.modalRef = this.modalService.show(template);
 }
 

submitted = false;


headElements = ['ID', 'Item', 'Select Quantity'];
headElements2 = ['Item', 'Category','Quantity'];

// itemm: HTMLInputElement = this.itemname.nativeElement.value;
// catt: HTMLElement = this.categoryi.nativeElement.value;
// quantity: HTMLElement = this.quantityreq.nativeElement.value;
elements: any = [
  {id: 1, first: 'Pencil Set', last: ''},
  {id: 2, first: 'Notebooks', last: ''},
  {id: 3, first: 'Textbooks', last: ''},
  {id: 4, first: 'Pen', last: ''},
  {id: 5, first: 'Bags', last: ''}
];
it : string;
cat: string;
qu: string;
ell: any;

onSubmit(){
  this.submitted=true;
  this.it = this.signupform.value.itemname;
  this.cat = this.signupform.value.category;
  this.qu = this.signupform.value.quantity;
  
  this.elements2.push({itemname: this.signupform.value.itemname,category: this.signupform.value.category,quantity: this.signupform.value.quantity});

  
 
// console.log(this.signupform);
}

onAdd(){
this.div1 = !this.div1;
}

onClose(){
  this.div1=false;
}
 }

