import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmrReqComponent } from './emr-req.component';

describe('EmrReqComponent', () => {
  let component: EmrReqComponent;
  let fixture: ComponentFixture<EmrReqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmrReqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmrReqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
