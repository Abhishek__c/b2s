import { Component, OnInit ,TemplateRef, ViewChild, ElementRef} from '@angular/core';
import {BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-emr-req',
  templateUrl: './emr-req.component.html',
  styleUrls: ['./emr-req.component.css']
})
export class EmrReqComponent implements OnInit {


  numbers: any;
  modalRef: BsModalRef;

  @ViewChild('itemname') itemname: ElementRef;
  @ViewChild('categoryi') categoryi: ElementRef;
  @ViewChild('quantityreq') quantityreq: ElementRef;
 id: string;
 first: any;
 last:any;
 elements2: any = [];

   constructor(private modalService: BsModalService) {
     this.numbers = new Array(10).fill(100).map((x,i)=>i); // [0,1,2,3,4,...,100]
   }
   ngOnInit(): void {

   }
   openModal(template: TemplateRef<any>) {
     this.modalRef = this.modalService.show(template);
 }
 elements: any = [
  {id: 1, first: 'Pencil Set', last: ''},
  {id: 2, first: 'Notebooks', last: ''},
  {id: 3, first: 'Textbooks', last: ''},
  {id: 4, first: 'Pen', last: ''},
  {id: 5, first: 'Bags', last: ''}
];




headElements = ['ID', 'Item', 'Select Quantity'];
headElements2 = ['Item', 'Category','Quantity'];

// itemm: HTMLInputElement = this.itemname.nativeElement.value;
// catt: HTMLElement = this.categoryi.nativeElement.value;
// quantity: HTMLElement = this.quantityreq.nativeElement.value;

onAdd(){

this.elements2.push(this.itemname.nativeElement.value,this.categoryi.nativeElement.value,this.quantityreq.nativeElement.value);


}
 }

