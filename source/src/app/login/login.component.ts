import { Component, OnInit } from '@angular/core';
import { ServicesService } from './../services.service';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { LayoutService } from '../layout.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin: boolean = false;
  message: any;
 
  showSpinner: boolean = true;
  isSpinnerVisibile$: Observable<boolean> = this.layoutService.isNavigationPending$;
  constructor(private formBuilder: FormBuilder,private apiService: ServicesService,private router:Router, private layoutService: LayoutService) {
  this.loginForm = this.formBuilder.group({
  email: ['', [Validators.required,Validators.minLength(1), Validators.email]],
  password: ['', Validators.required]
  });
  }

  ngOnInit(): void {
  } 
onSubmit(){
console.log(this.loginForm.value); 
if(this.loginForm.invalid){
  return;
}

const loginData = {
  email: this.loginForm.controls.email.value,
  password: this.loginForm.controls.password.value
};

this.apiService.login(loginData).subscribe((data: any) =>  {
  // const redirect = this.apiService.redirectUrl ? this.apiService.redirectUrl : '/rhme';
  // this.router.navigate([redirect]);
  // },
  // error => {
  //alert("User name or password is incorrect")
   //console.log(data.token);
  if(data.token) {
    window.localStorage.setItem('token', data.token);
    window.localStorage.setItem('name', data.name);
    window.localStorage.setItem('email_id', data.email_id);
    window.localStorage.setItem('phn_no', data.phn_no);
    window.localStorage.setItem('syl', data.syl);
    window.localStorage.setItem('inst_phnno', data.inst_phnno);
    window.localStorage.setItem('inst_add', data.inst_add);
    this.router.navigate(['rhme']);
 } 
 else {
   this.invalidLogin = true;
  // alert('a' + data.message);
 }
});
}

}
