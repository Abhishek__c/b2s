import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NwDnrComponent } from './nw-dnr.component';

describe('NwDnrComponent', () => {
  let component: NwDnrComponent;
  let fixture: ComponentFixture<NwDnrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NwDnrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NwDnrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
