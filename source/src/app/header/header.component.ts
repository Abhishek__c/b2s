;
import { Router, NavigationStart, NavigationEnd, RouterEvent } from '@angular/router';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showLoadingIndicator = true;
  showMenu: boolean;
  constructor(private router: Router) { 
    this.router.events.subscribe((routerEvent: RouterEvent) => {

      if(routerEvent instanceof NavigationStart){
        this.showLoadingIndicator = true;
      }

      if(routerEvent instanceof NavigationEnd){
        this.showLoadingIndicator = false;
      }

     });
     
  }

  ngOnInit(): void {
  }

}
