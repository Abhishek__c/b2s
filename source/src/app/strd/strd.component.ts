import { FormGroup, FormBuilder} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ServicesService } from './../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-strd',
  templateUrl: './strd.component.html',
  styleUrls: ['./strd.component.css']
})
export class StrdComponent implements OnInit {
  addStrd: FormGroup;

  constructor( private formBuilder: FormBuilder, private apiService: ServicesService, private router: Router) { }

  ngOnInit(): void {
    
    this.addStrd = this.formBuilder.group({
      grade: [],
      number: [] })
  }

  
  
  onSubmit(){
    
  const strdData = {
      grade: this.addStrd.controls.grade.value,
      number: this.addStrd.controls.number.value
    };

    console.log(this.addStrd.value);
   
    this.apiService.addGrade(strdData)
     .subscribe( data => {

       window.localStorage.setItem('strd', data.strd);
       window.localStorage.setItem('num', data.num);

       this.router.navigate(['request']);

       });

    

  }

}
