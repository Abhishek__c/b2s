import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrdComponent } from './strd.component';

describe('StrdComponent', () => {
  let component: StrdComponent;
  let fixture: ComponentFixture<StrdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
