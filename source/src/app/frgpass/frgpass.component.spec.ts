import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrgpassComponent } from './frgpass.component';

describe('FrgpassComponent', () => {
  let component: FrgpassComponent;
  let fixture: ComponentFixture<FrgpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrgpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrgpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
