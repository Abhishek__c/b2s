import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqStsComponent } from './req-sts.component';

describe('ReqStsComponent', () => {
  let component: ReqStsComponent;
  let fixture: ComponentFixture<ReqStsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqStsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqStsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
