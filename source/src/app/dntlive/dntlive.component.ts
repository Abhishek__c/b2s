import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService,BsModalRef} from 'ngx-bootstrap/modal'
import { from } from 'rxjs';
import { Template } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-dntlive',
  templateUrl: './dntlive.component.html',
  styleUrls: ['./dntlive.component.css']
})
export class DntliveComponent implements OnInit {
modalRef: BsModalRef;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
openModal(template:TemplateRef<any>)
{
  this.modalRef=this.modalService.show(template);
}
}
