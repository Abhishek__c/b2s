import { User } from './Model/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from './Model/api-response';

interface myData{
   message: string;
   success: string;
}

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) { }
  baseUrl ='http://localhost/AngularLoginRegister';

  redirectUrl: string;
  login(loginData): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + '/login.php', loginData);
}

  createUser(user: User): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + '/insert.php', user);
  }

  addGrade(strdData): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + '/request.php', strdData);
  }

}
