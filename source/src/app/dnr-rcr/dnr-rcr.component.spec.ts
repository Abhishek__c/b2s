import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DnrRcrComponent } from './dnr-rcr.component';

describe('DnrRcrComponent', () => {
  let component: DnrRcrComponent;
  let fixture: ComponentFixture<DnrRcrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DnrRcrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnrRcrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
