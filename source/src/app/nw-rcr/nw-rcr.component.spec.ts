import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NwRcrComponent } from './nw-rcr.component';

describe('NwRcrComponent', () => {
  let component: NwRcrComponent;
  let fixture: ComponentFixture<NwRcrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NwRcrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NwRcrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
