import {Event, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { ServicesService } from './../services.service';
import { NgForm, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CustomvalidationService } from '../services/customvalidation.service';

@Component({
  selector: 'app-nw-rcr',
  templateUrl: './nw-rcr.component.html',
  styleUrls: ['./nw-rcr.component.css']
})
export class NwRcrComponent implements OnInit {
  passwordIsValid = false;
  pp = false;
  addForm: FormGroup;
  token:any;
  showLoadingIndicator = true;
  constructor(private formBuilder: FormBuilder,
   private apiService: ServicesService,
   private router: Router,  private customValidator: CustomvalidationService) { 
     this.router.events.subscribe((routerEvent: Event) => {

      if(routerEvent instanceof NavigationStart){
        this.showLoadingIndicator = true;
      }

      if(routerEvent instanceof NavigationEnd){
        this.showLoadingIndicator = false;
      }

     });
   }
   
 ngOnInit(): void {

  
   this.addForm = this.formBuilder.group({
     email: ['',[Validators.required, Validators.email]],
     mobno: [],
     fname: [],
     lname: [],
     password: ['', [Validators.required,Validators.maxLength(20),Validators.minLength(6)]],
     confirmpassword: ['', [Validators.required, Validators.maxLength(20),Validators.minLength(6)]],
     syllabus: [],
     instname: [],
     instphone: [],
     instmail: [],
     instaddress: [],
     

   }, {
        validator: this.customValidator.MatchPassword('password', 'confirmpassword'),
      });
 }
 get addFormControl() {
  return this.addForm.controls;
}
onSubmit(){
 

 this.apiService.createUser(this.addForm.value)
     .subscribe( data => {
       this.router.navigate(['']);
     });
}

passwordValid(event) {
  this.passwordIsValid = event;
  this.pp = true;
}


}
