
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { FrgpassComponent } from './frgpass/frgpass.component';
import { DnrRcrComponent } from './dnr-rcr/dnr-rcr.component';
import { NwDnrComponent } from './nw-dnr/nw-dnr.component';
import { NwRcrComponent } from './nw-rcr/nw-rcr.component';
import { ProfileComponent } from './profile/profile.component';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { RqstComponent } from './rqst/rqst.component';
import { DsplitComponent } from './dsplit/dsplit.component';
import { DhmeComponent } from './dhme/dhme.component';
import { DnavComponent } from './dnav/dnav.component';
import { DntliveComponent } from './dntlive/dntlive.component';
import { RhmeComponent } from './rhme/rhme.component';
import { RnavComponent } from './rnav/rnav.component';
import { RprofileComponent } from './rprofile/rprofile.component';
import { RequestComponent } from './request/request.component';
import { EmrReqComponent } from './emr-req/emr-req.component';
import { ReqSplitComponent } from './req-split/req-split.component';
import { CustomerComponent } from './customer/customer.component';
import { RqstStsComponent } from './rqst-sts/rqst-sts.component';
import { RqHistComponent } from './rq-hist/rq-hist.component';
import { StrdComponent } from './strd/strd.component';


const routes: Routes = [{path: 'login',component: LoginComponent},
{path: 'frgpass',component:FrgpassComponent},
{path: 'dnr-rcr',component:DnrRcrComponent},{path:'nw-dnr',component:NwDnrComponent},
{path:'nw-rcr',component:NwRcrComponent},{path:'profile',component:ProfileComponent},
{path: '',component:MainComponent},{path:'header',component:HeaderComponent},{path:'rqst',component:RqstComponent},
{path:'dsplit',component:DsplitComponent},{path:'dhme',component:DhmeComponent},{path:'dnav',component:DnavComponent},
{path:'dntlive',component:DntliveComponent},{path:'rhme', component:RhmeComponent},{path:'rnav',component:RnavComponent}
,{path:'rprofile' , component:RprofileComponent},{path:'request', component:RequestComponent},{path:'emr-req',component:EmrReqComponent}
,{path:'req-split',component:ReqSplitComponent},{path:'customer',component:CustomerComponent},{path:'rqst-sts',component:RqstStsComponent},
{path:'rq-hist',component:RqHistComponent},{path:'strd',component:StrdComponent}];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


export const routingcomponents=[LoginComponent,FrgpassComponent,DnrRcrComponent,
  NwDnrComponent,NwRcrComponent,ProfileComponent,MainComponent,HeaderComponent,
  RqstComponent,DsplitComponent,DhmeComponent,DnavComponent,DntliveComponent,RhmeComponent,RnavComponent,RprofileComponent,
  RequestComponent,EmrReqComponent,ReqSplitComponent,CustomerComponent,RqstStsComponent,RqHistComponent,StrdComponent,]
