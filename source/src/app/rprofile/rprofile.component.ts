import { Component, OnInit } from '@angular/core';
import { ServicesService } from './../services.service';
@Component({
  selector: 'app-rprofile',
  templateUrl: './rprofile.component.html',
  styleUrls: ['./rprofile.component.css']
})
export class RprofileComponent implements OnInit {

  constructor(private apiService: ServicesService) { }
  name: any;
  email_id: any;
  phn_no : any;
  syl: any;
  inst_phnno: any;
  inst_add: any;

  ngOnInit(): void {
    this.name = window.localStorage.getItem('name');
    this.email_id = window.localStorage.getItem('emai_id');
    this.phn_no = window.localStorage.getItem('phn_no');
    this.syl = window.localStorage.getItem('syl');
    this.inst_phnno = window.localStorage.getItem('inst_phnno');
    this.inst_add = window.localStorage.getItem('inst_add');
    
  }

}
