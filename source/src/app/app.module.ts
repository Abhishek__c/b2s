import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule , routingcomponents} from './app-routing.module';
import { AppComponent } from './app.component';
import{ModalModule} from 'ngx-bootstrap/modal';
import { StsHisComponent } from './rqst-sts/sts-his/sts-his.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';
import { PasswordStrengthComponent } from './password-strength/password-strength.component';

 
import { LayoutService } from './layout.service';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    routingcomponents,
    StsHisComponent,
    PasswordStrengthComponent,
    
    LoaderComponent,

  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    FormsModule,ReactiveFormsModule,
    HttpClientModule,
    PasswordStrengthMeterModule,
   
  ],
  providers: [
    LayoutService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
