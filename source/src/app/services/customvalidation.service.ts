import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomvalidationService {


   
  
    MatchPassword(password: string, confirmpassword: string) {
      return (formGroup: FormGroup) => {
        const passwordControl = formGroup.controls[password];
        const confirmpasswordControl = formGroup.controls[confirmpassword];
  
        if (!passwordControl || !confirmpasswordControl) {
          return null;
        }
  
        if (confirmpasswordControl.errors && !confirmpasswordControl.errors.passwordMismatch) {
          return null;
        }
  
        if (passwordControl.value !== confirmpasswordControl.value) {
          confirmpasswordControl.setErrors({ passwordMismatch: true });
        } else {
          confirmpasswordControl.setErrors(null);
        }
      }
    }
  }
