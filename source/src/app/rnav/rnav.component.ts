
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from './../services.service';
@Component({
  selector: 'app-rnav',
  templateUrl: './rnav.component.html',
  styleUrls: ['./rnav.component.css']
})
export class RnavComponent implements OnInit {
   
  constructor(private router: Router, private service: ServicesService) { }
  token:any;
  name:any;
  email_id: any;
  phn_no : any;
  syl: any;
  inst_phnno: any;
  inst_add: any;
  ngOnInit(): void {
    this.name = window.localStorage.getItem('name');
    console.log(this.name);

    
  }


  Logout(){
    this.token = window.localStorage.removeItem('token');
    this.name = window.localStorage.removeItem('name');
    this.email_id = window.localStorage.removeItem('email_id');
    this.phn_no = window.localStorage.removeItem('phn_no');
    this.syl = window.localStorage.removeItem('syl');
    this.inst_phnno = window.localStorage.removeItem('inst_phnno');
    this.inst_add = window.localStorage.removeItem('inst_add');
    this.router.navigate(['login']);
  }
}



