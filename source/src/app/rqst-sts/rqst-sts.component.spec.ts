import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RqstStsComponent } from './rqst-sts.component';

describe('RqstStsComponent', () => {
  let component: RqstStsComponent;
  let fixture: ComponentFixture<RqstStsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RqstStsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RqstStsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
