import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StsHisComponent } from './sts-his.component';

describe('StsHisComponent', () => {
  let component: StsHisComponent;
  let fixture: ComponentFixture<StsHisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StsHisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StsHisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
