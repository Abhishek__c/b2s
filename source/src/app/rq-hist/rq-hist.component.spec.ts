import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RqHistComponent } from './rq-hist.component';

describe('RqHistComponent', () => {
  let component: RqHistComponent;
  let fixture: ComponentFixture<RqHistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RqHistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RqHistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
