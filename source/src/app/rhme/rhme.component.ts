import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from './../services.service';

@Component({
  selector: 'app-rhme',
  templateUrl: './rhme.component.html',
  styleUrls: ['./rhme.component.css']
})
export class RhmeComponent implements OnInit {
   
  message ="Loading";
  constructor(private router: Router, private apiService: ServicesService) { }

  token: any;
  ngOnInit(): void {
    this.token = window.localStorage.getItem('token');
    console.log(this.token);
    if(!this.token){
      this.router.navigate(['login']);
    }

    
  }


}
